from socket import *
from threading import *
from tkinter import *
from config import *

class Main():

    def main(self):

        client = socket(AF_INET, SOCK_STREAM)
        ip = HOSTNAME
        port = PORTS

        client.connect((ip, port))

        window = Tk()
        window.title('Connected to :' + ip + ":" + str(port))

        mainLabel = Label(window, text='Retto Client Example', height=15)
        mainLabel.grid(row=0, column=0, padx=10, pady=10)

        messages = Text(window, width=50)
        messages.grid(row=1, column=1, padx=10, pady=10)

        yourMessage = Entry(window, width=50)
        yourMessage.insert(0, 'Enter your username')
        yourMessage.grid(row=1, column=0, padx=10, pady=10)
        yourMessage.focus()
        yourMessage.selection_range(0, END)

        def sendMessage():
            clientMessage = yourMessage.get()
            messages.insert(END, '\n' + 'You: ' + clientMessage)
            client.send(clientMessage.encode('utf8'))
            yourMessage.delete(0, END)

        messageEvent = Button(window, text='Gönder', width=20, command=sendMessage)
        messageEvent.grid(row=2, column=0, padx=10, pady=10)

        def recvMessage():
            while True:
                serverMessage = client.recv(1024).decode('utf8')
                messages.insert(END, '\n' + serverMessage)

        recvThread = Thread(target=recvMessage)
        recvThread.daemon = True
        recvThread.start()

        window.mainloop()

    if __name__ == '__main__':
        main()